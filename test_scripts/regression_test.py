import requests
import base64
import uuid

HOST = 'http://localhost:5000'

class Helper:
    @staticmethod
    def _registration(body, code=200, **kwargs):
        api = '/registration'

        url = f'{HOST}{api}'
        # Additional headers.
        headers = {'Content-Type': 'application/json',
                   }
        # Body
        payload = body
        # convert dict to json string by json.dumps() for body data.
        resp = requests.post(url, headers=headers, json=payload)

        # Validate response headers and body contents, e.g. status code.
        assert resp.status_code == code
        if 'message' in kwargs:
            resp_body = resp.json()
            assert resp_body['message'] == kwargs['message']

    @staticmethod
    def _login(username, password, code=200, no_headers=False, **kwargs):
        api = '/login'

        url = f'{HOST}{api}'
        # Additional headers.
        headers = {'Content-Type': 'application/json',
                   'Authorization': 'Basic ' + base64.b64encode(bytes(username + \
                                                                      ":" + password, 'ascii')).decode(
                       'ascii')
                   }
        headers = {} if no_headers else headers
        # convert dict to json string by json.dumps() for body data.
        resp = requests.post(url, headers=headers)

        # Validate response headers and body contents, e.g. status code.
        assert resp.status_code == code
        if 'message' in kwargs:
            resp_body = resp.json()
            assert resp_body['message'] == kwargs['message']

    @staticmethod
    def _change_pwd(username, password, new_password='', code=200, **kwargs):
        api = '/change_pwd'

        url = f'{HOST}{api}'
        # Additional headers.
        headers = {'Content-Type': 'application/json',
                   'Authorization': 'Basic ' + base64.b64encode(bytes(username + \
                                                                      ":" + password, 'ascii')).decode(
                       'ascii')
                   }
        # Body
        if new_password:
            payload = {'new_password': new_password}
        else:
            payload = {}
        # convert dict to json string by json.dumps() for body data.
        resp = requests.post(url, headers=headers, json=payload)

        # Validate response headers and body contents, e.g. status code.
        assert resp.status_code == code
        if 'message' in kwargs:
            resp_body = resp.json()
            assert resp_body['message'] == kwargs['message']

    @staticmethod
    def exec_test(case):
        method = case['method']
        param = case['params']
        method(**param)

## TEST CASES
test_email = f'User{str(uuid.uuid1())[:4]}@examle.com'

def test_aa1():
    case = {
        'method': Helper._registration,
        'params': {'body': {'email': test_email, 'password': 'Password1'}, 'code': 201,
                   'message': 'New account has been successfully created!'}}
    Helper.exec_test(case)


def test_aa2():
    case = {
        'method': Helper._login,
        'params': {'username': test_email, 'password': 'Password1', 'code': 200, 'no_headers': 0}}
    Helper.exec_test(case)


def test_aa3():
    case = {
        'method': Helper._change_pwd,
        'params': {'username': test_email, 'password': 'Password1', 'new_password': 'Password223', 'code': 200,
                   'message': 'Password successfully changed!'}}
    Helper.exec_test(case)


def test_aa4():
    case = {
        'method': Helper._registration,
        'params': {'body': {'email': test_email, 'password': 'Password1'}, 'code': 409,
                   'message': 'An account with this email already exists!'}}
    Helper.exec_test(case)


def test_aa5():
    case = {
        'method': Helper._registration,
        'params': {'body': {'email': 'newUser100', 'password': 'Password1'}, 'code': 422,
                   'message': 'Email validation error'}}
    Helper.exec_test(case)


def test_aa6():
    case = {
        'method': Helper._registration,
        'params': {'body': {'email': 'new200@examle.com', 'password': 'pas'}, 'code': 409,
                   'message': 'Password validation error.'}}
    Helper.exec_test(case)


def test_aa7():
    case = {
        'method': Helper._registration,
        'params': {'body': {'email': 'new200@examle.com'}, 'code': 400, 'message': "The request had bad syntax."}}
    Helper.exec_test(case)


def test_aa8():
    case = {
        'method': Helper._login,
        'params': {'username': test_email, 'password': 'Password1', 'code': 400, 'no_headers': 1,
                   'message': "Authentication required. Please provide email and password!"}}
    Helper.exec_test(case)


def test_aa9():
    case = {
        'method': Helper._login,
        'params': {'username': 'UserDosntExist@examle.com', 'password': 'Password1', 'code': 401, 'no_headers': 0,
                   'message': "User doesn't exist!"}}
    Helper.exec_test(case)


def test_aa10():
    case = {
        'method': Helper._login,
        'params': {'username': test_email, 'password': 'Password2231', 'code': 401, 'no_headers': 0,
                   'message': "Invalid password!"}}
    Helper.exec_test(case)


def test_aa11():
    case = {
        'method': Helper._change_pwd,
        'params': {'username': test_email, 'password': 'Password223', 'code': 400,
                   'message': "The request had bad syntax."}}
    Helper.exec_test(case)


def test_aa12():
    case = {
        'method': Helper._change_pwd,
        'params': {'username': test_email, 'password': 'Password223', 'new_password': 'pas', 'code': 409,
                   'message': 'Password validation error.'}}
    Helper.exec_test(case)
