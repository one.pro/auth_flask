# Automated regression tests

### 100% APIs are covered

### Commands to execute tests

`$ cd test_scripts/`

Build python environment:

  On macOS and Linux:
- `$ python3 -m venv env` 


  On Windows:
-   `py -m venv env`

#
Activate env:

`$ source env/bin/activate`

Install requirements:

`$ pip install -r requirements.txt`

Execute test:

`$ pytest regression_test.py`
