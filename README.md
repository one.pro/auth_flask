## auth_flask 

**Authentication server with JWT token (Python, Flask, Docker)**

*automated tests included in `test_scripts/`

### Commands

Build and run:

`git clone https://gitlab.com/one.pro/auth_flask.git` 
or download and unzip the directory

`cd auth_flask`

`docker-compose up --build`

### Additional commands

-   _Run in backround:_

    `docker-compose up --build -d`

-   _View logs for all services_:

    `docker-compose logs --tail=all -f`

-   _Stop all services:_

    `docker-compose down`

-   _Stop and Delete all containers:_

    `docker container stop $(docker container ls -aq) && docker container rm $(docker container ls -aq)`
    

### Available APIs
- `/registration`
- `/login`
- `/change_pwd`
