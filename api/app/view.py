from flask import request, jsonify, make_response
from functools import wraps
from flask import current_app as app
from app.helper import valid_email, password_validation
import datetime as dt
from app.models import db, User
from werkzeug.security import check_password_hash, generate_password_hash
import jwt
from calendar import timegm

def authorized(f):
   """This decorator handels login process."""
   @wraps(f)
   def decorated_function(*args, **kws):
      auth_data = request.authorization

      if not auth_data or not auth_data['password'] or not auth_data['username']:
         return jsonify({'message': "Authentication required. Please provide email and password!"}), 400

      password = auth_data['password']
      email = auth_data['username']

      # Find user by email
      user = User.find_by_email(email)
      if not isinstance(user, User):
         return jsonify({'message': "User doesn't exist!"}), 401

      # Verify password
      if not user.verify_password(password):
         return jsonify({'message': "Invalid password!"}), 401
      # If user exist and verified
      return f(user, *args, **kws)
   return decorated_function


@app.route('/registration', methods=['POST'])
def signup_user():
   """This API creats a new user
      'email' - required field
      'password' - required field
   """
   data = request.get_json()
   if data and 'email' in data and 'password' in data:
      email = str(data['email']).lower()
      password = data['password']

      # Validation
      # First. Email validation format
      if not valid_email(email):
         return jsonify({'message':'Email validation error', 'email':'Is not valid'}), 422

      # Second. Cheking if user already exist
      user = User.find_by_email(email=email)
      if user:
         return jsonify({'message':'An account with this email already exists!'}), 409

      # Third. Password validation.
      result_pwd = password_validation(password)
      if result_pwd:
         result_pwd['message'] = 'Password validation error.'
         return jsonify(result_pwd), 409

      # Creating a new user
      #pass_hash = generate_password_hash(password)
      User.create(email=email, password=password)

      return jsonify({'message':'New account has been successfully created!'}), 201
   else:
      # in case "password" or "email" dont exist
      return jsonify({'message':"The request had bad syntax.",
                      'email':'Required field',
                      'password':'Required field'}), 400


@app.route('/login', methods=['POST'])
@authorized
def login(user):
   """This API returns jwt token by user credentials"""

   # Converting date into json serializable format
   created_date = timegm(dt.datetime.utcnow().utctimetuple())
   expired_date = timegm((dt.datetime.utcnow()+dt.timedelta(hours=1)).utctimetuple())

   # Creating JWT token
   token = jwt.encode({'userId': user.id, 'created': created_date , 'expires':expired_date},
                      app.config['SECRET_KEY'])

   return jsonify({'token': token.decode('UTF-8')}), 200


@app.route('/change_pwd', methods=['POST'])
@authorized
def password_change(user):
   """This API change pass
      'new_password' - required field
   """
   data = request.get_json()

   # Checking required fields
   if not data or not 'new_password' in data:
      return jsonify({'message':"The request had bad syntax.",
                      'new_password':'Required field'}), 400

   new_password = data['new_password']

   # Checkin if new password is valid
   result_pwd = password_validation(new_password)
   if result_pwd:
      result_pwd['new_password'] = result_pwd['password']
      result_pwd['message'] = 'Password validation error.'
      del result_pwd['password']
      return jsonify(result_pwd), 409

   #Changing password
   user.change_password(new_password)

   return jsonify({'message': 'Password successfully changed!'}), 200
