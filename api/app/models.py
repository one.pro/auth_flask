import uuid
from app import db
from werkzeug.security import check_password_hash, generate_password_hash

class User(db.Model):
   id = db.Column(db.String(100), primary_key=True, default=lambda: str(uuid.uuid4()))
   email = db.Column(db.String(50), unique=True)
   password = db.Column(db.String(250))

   def __str__(self):
      return self.email

   def hash_password(self, password):
      self.password = generate_password_hash(password, salt_length=12)

   def verify_password(self, password):
      return check_password_hash(self.password, password)

   def change_password(self, new_password):
      self.hash_password(new_password)
      db.session.commit()

   @classmethod
   def find_by_email(cls, email):
      user = cls.query.filter_by(email=email).first()
      if user:
         return user
      return None

   @classmethod
   def create(cls, **kw):
      obj = cls(**kw)
      obj.hash_password(kw['password'])
      db.session.add(obj)
      db.session.commit()
      return obj
