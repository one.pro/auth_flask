import re
from app.models import User

def valid_email(email):
    """This method validates an Email format by regular expression """
    regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'
    return True if re.search(regex, email) else False

def password_validation(passwrd):
    """This method validates password
        Requirements:
        - 6 < length < 20
        - one number, one uppercase, one lowercase
    """
    result = {}

    if len(passwrd) < 6:
        result['password'] = 'length should be at least 6'

    if len(passwrd) > 20:
        result['password'] ='length should be not be greater than 20'

    if not any(char.isdigit() for char in passwrd):
        result['password'] = 'Password should have at least one numeral'

    if not any(char.isupper() for char in passwrd):
        result['password'] = 'Password should have at least one uppercase letter'

    if not any(char.islower() for char in passwrd):
        result['password'] = 'Password should have at least one lowercase letter'

    return result