from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import logging
db = SQLAlchemy()

def create_app(testing=False):
    """Config setup"""
    app = Flask(__name__)
    # Config loading
    if app.config['ENV'] == 'development':
        app.config.from_object('config.DevConfig')
    elif app.config['ENV'] == 'production':
        app.config.from_object('config.ProdConfig')
    else:
        app.config.from_object('config.TestConfig')

    db.init_app(app)

    with app.app_context():
        from app import view
        db.create_all()  # Create sql tables for our data models


    return app