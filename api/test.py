import os
import base64
import unittest
unittest.TestLoader.sortTestMethodsUsing = None
import json
from app.models import User
from app import create_app, db

# Settings
test_app = create_app()
# set up the application context
with test_app.app_context():
    # clean old records
    db.drop_all()
    # Create sql tables for our data models
    db.create_all()

positive_reg_data = {'email':'email@username.loc', 'password':'Password123', 'new_password':'NewPass456'}

class UserAPITests(unittest.TestCase):
    """Base regression test"""

    # setup environment
    def setUp(self):
        self.app = test_app.test_client()
        self.assertEqual(test_app.debug, False)

    # Helper methods #
    def _registration(self, code=201):
        response = self.app.post(
            '/registration',
            json={'email':positive_reg_data['email'], 'password':positive_reg_data['password']},
            follow_redirects=True
        )
        self.assertEqual(response.status_code, code)

        if response.status_code == 201:
            # user created in DB
            with test_app.app_context():
                user = User.find_by_email(email=positive_reg_data['email'])
                self.assertIsInstance(user, User)

    def _login(self, code=200):

        response = self.app.post(
            '/login',
            headers={
                'Authorization': 'Basic ' + base64.b64encode(bytes(positive_reg_data['email'] + \
                                                                   ":" + positive_reg_data['password'], 'ascii')).decode('ascii')
            },
            follow_redirects=True
        )
        self.assertEqual(response.status_code, code)

    def _change_pwd(self, code=200):
        response = self.app.post(
            '/change_pwd',
            headers={
                'Authorization': 'Basic ' + base64.b64encode(bytes(positive_reg_data['email'] + \
                                                                   ":" + positive_reg_data['password'],
                                                                   'ascii')).decode('ascii')
            },
            json={'new_password':positive_reg_data['new_password']},
            follow_redirects=True
        )
        self.assertEqual(response.status_code, code)


    # TEST POSITIVE #
    def test_a1_registration(self):
        self._registration()

    def test_a2_login(self):
        self._login()

    def test_a3_change_pwd(self):
        self._change_pwd()

    # TESTS NEGATIVE #
    def test_b1_registration(self):
        self._registration(code=409)

    def test_b2_login(self):
        self._login(code=401)


if __name__ == "__main__":
    unittest.main()
