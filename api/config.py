import pathlib

BASE_PATH = str(pathlib.Path(__file__).parent.absolute())

class BaseConfig():
   TESTING = False
   DEBUG = False
   SECRET_KEY = 'AAAAC3Nza48dn2dkTE5AAAAIGMxZTqMtbvM4cW7p+yCkVme+'
   SQLALCHEMY_TRACK_MODIFICATIONS = True

class DevConfig(BaseConfig):
   DEBUG = True
   SQLALCHEMY_DATABASE_URI = f'sqlite:///{BASE_PATH}/dev.db'

class ProdConfig(BaseConfig):
   SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://dbuser:dbpassword@db/quotes_db'

class TestConfig(BaseConfig):
   SQLALCHEMY_DATABASE_URI = f'sqlite:///{BASE_PATH}/test_db.db'
   TESTING = True
   DEBUG = False